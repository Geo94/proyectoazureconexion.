create procedure ingresar_factura
@serie varchar(100), @numero int, 
@nombre varchar(200), @direccion varchar(200), @total money as
insert into factura (serie, numero, fecha, nombre, direccion, total) 
values (@serie, @numero, getdate(), @nombre, @direccion, @total)
go

create procedure detalle_factura
@serie varchar(100), @numero int, @cantidad int, @producto varchar(100)
, @precio money as insert into detallefactura (serie, numero, cantidad
, producto, precio)
values (@serie, @numero, @cantidad, @producto, @precio)
go 