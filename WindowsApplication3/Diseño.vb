﻿Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Public Class IngresarFactura
    '"Data source=George; Initial Catalog=Northwind; Persist Security Info=True; User ID=SA; Password=P@ssw0rd"


    '@serie varchar(5), @numero varchar(100), @fecha varchar(200), 
    '@nombre varchar(100), @direccion varchar(100), @total money 
    Private Sub BTNGenerar_Click(sender As Object, e As EventArgs) Handles BTNGenerar.Click
        Try
            Dim conexion = DatabaseFactory.CreateDatabase("conexion") 'databasefactory es una conexion automatica'
            Dim comandoSQL As New SqlClient.SqlCommand
            comandoSQL = conexion.GetStoredProcCommand("dbo.ingresar_factura")
            comandoSQL.Parameters.Add("@serie", SqlDbType.NChar).Value = Serie.Text
            comandoSQL.Parameters.Add("@numero", SqlDbType.Int).Value = Numero.Text
            comandoSQL.Parameters.Add("@nombre", SqlDbType.NChar).Value = Nombre.Text
            comandoSQL.Parameters.Add("@direccion", SqlDbType.NChar).Value = Direccion.Text
            comandoSQL.Parameters.Add("@total", SqlDbType.Money).Value = Total.Text
            conexion.ExecuteNonQuery(comandoSQL)

            For Each row As DataGridViewRow In Data.Rows
                If row.Cells(0).Value <> 0 And row.Cells(1).Value <> "" And row.Cells(2).Value <> 0 Then
                    comandoSQL = conexion.GetStoredProcCommand("dbo.detalle_factura")
                    comandoSQL.Parameters.Add("@serie", SqlDbType.NChar).Value = Serie.Text
                    comandoSQL.Parameters.Add("@numero", SqlDbType.Int).Value = Numero.Text
                    comandoSQL.Parameters.Add("@cantidad", SqlDbType.Int).Value = row.Cells(0).Value
                    comandoSQL.Parameters.Add("@producto", SqlDbType.NChar).Value = row.Cells(1).Value
                    comandoSQL.Parameters.Add("@precio", SqlDbType.Money).Value = row.Cells(2).Value
                    conexion.ExecuteNonQuery(comandoSQL)
                End If
            Next
                    conexion.CreateConnection.Close()
                    MsgBox("Ya se ingresaron los datos del cliente")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub
    Private Sub limpiarforma()
        Try
            For Each ocontrol1 As Control In Me.Controls
                If TypeOf ocontrol1 Is TextBox Then
                    ocontrol1.Text = ""
                End If
            Next
        Catch ex As Exception
            MessageBox.Show("error de" & ex.Message)
        End Try
    End Sub

  
End Class



