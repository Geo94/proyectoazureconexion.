create database FacturaCliente
go

use facturacliente

create table Factura (
serie varchar(100),
numero int, 
fecha date,
nombre varchar(200),
direccion varchar(200),
total money,
constraint pk_factura primary key (numero,serie)
)
go

create table detallefactura (
serie varchar(100), 
numero int, 
cantidad int,
producto varchar(200),
precio money,
constraint fk_detallefactura foreign key(numero,serie) 
references factura (numero,serie)
)
go







